import color from './Color';
import {Dimensions, StyleSheet, Platform, BackHandler} from 'react-native';
const Height = Dimensions.get('screen').height;
const Width = Dimensions.get('screen').width;

const Styles = StyleSheet.create({
  //-----------------GLOBAL-----------------//
  container: {
    flex: 1,
    backgroundColor: color.white,
  },
 
  rowtotal: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
  },
 
  box:{
    width: '90%',
    height: 40,
    backgroundColor:color.lightwhite,
    alignSelf: 'center',
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems:'center',
    borderRadius:5
  },

 margintop:{
  marginTop:'10%'
 },
 
 margintop2:{
  marginTop:'5%'
 },

 alignsef:{
   alignSelf:'center'
 },
 box2:{
  width:'20%',height:40,backgroundColor:color.lightwhite,justifyContent:'center',alignItems:'center'
 },


 box3:{
  width:'70%',backgroundColor:color.lightwhite
 },

 btn:{
  width:'60%',height:45,backgroundColor:color.lightwhite,borderRadius:10,alignSelf:'center',
  justifyContent:'center',
  alignItems:'center'
 }

});

export default Styles;
