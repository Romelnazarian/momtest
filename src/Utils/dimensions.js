import {PixelRatio, Platform, StatusBar, Dimensions} from 'react-native';
const wholeheight = Dimensions.get('screen').height;
const screenheight = Dimensions.get('window').height;
// import DeviceInfo from 'react-native-device-info';
//DeviceInfo.hasNotch() -> check line 11
const statusbar = StatusBar.currentHeight;
const navbar = wholeheight - screenheight + statusbar;
const myWidth = Dimensions.get('window').width;
let myHeight = screenheight;
if (Platform.OS === 'android') {
  if (statusbar > 24) {
    myHeight = screenheight - statusbar;
  } else {
    myHeight = wholeheight - statusbar;
  }
}
const width = i => {
  return PixelRatio.roundToNearestPixel((myWidth * i) / 100);
};
const height = i => {
  return PixelRatio.roundToNearestPixel((myHeight * i) / 100);
};
const totalSize = i =>
  PixelRatio.roundToNearestPixel(
    Math.sqrt(myHeight ** 2 + myWidth ** 2) * (i / 100),
  );

export {width, height, totalSize};
