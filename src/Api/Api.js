import axios from 'axios';
import {BaseUrl} from './ServerUrl';

const config = (method, url,data, token) => {
  if (method == 'get')
    return {
      method: method,
      url: `${baseUrl}${url}`,
    };
  else
    return {
      method: method,
      url: `${BaseUrl}${url}`,
      data: data,
    };
};

export const callback =(callvalue)=>{
  // console.warn(callvalue)
  callvalue
}
export const ApiCall = async (
  method,
  url,
  data,
  name = 'response ->',
  token = null,
  callback,
  onError,
) => {
  try {
    const response = await axios(config(method, url, data, token,));
    if (response.status == 200 || response.status == 201) {
      const res = response.data;
      console.log(
        `================================================================================================`,
      );
      console.log(`===============${name}===============`);
      console.log('res',res);
      console.log(`===========End Of ${name}=====================`);
      console.log(
        `================================================================================================`,
      );
      if (
        res.isActive == true ||
        res.statusCode == 200 ||
        res.statusCode == 201 ||
        res.statusCode == 'successful'||
        res.statusCode == undefined

        
      ) {
        if (typeof callback == 'function') callback(res);
      } else {
        //Failed

        if (typeof onError == 'function') onError(response);
      }
    } else {
      // Status != 200
      if (typeof onError == 'function') onError(response);
    }
  } catch (error) {
    if (typeof onError == 'function') onError(error);

    if (error && error.message == 'Network Error')
      showToast('لطفا اتصال اینترنت خود را بررسی نمایید', color.red);
  }
};


// export const ApiCall = async (
//   method,
//   url,
//   data,
//   token = null,
// ) => {
//   try {
//     const response = await axios(config(method, url,data, token));
//     if (response.data.act == 'Success') {
//       const res = response.data;
//       // console.warn(res)
//      callback(res)
//     }
//   } catch (error) {
//     if (typeof onError == 'function') onError(error);

//     if (error && error.message == 'Network Error')
//       showToast('لطفا اتصال اینترنت خود را بررسی نمایید', color.red);
//   }
// };

