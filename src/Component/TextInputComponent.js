import React from 'react';
import {View, Text, TextInput} from 'react-native';
import Styles from '../Utils/Styles';
import color from '../Utils/Color';

const TextInputComponent = props => {
  return (
    <TextInput
      style={[Styles.box3, {...props.inputstyle}]}
      placeholder="Code"
      onChangeText={val => props.changetextval(val)}
      keyboardType="numeric"
    />
  );
};

export default TextInputComponent;
