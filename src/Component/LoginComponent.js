import React,{useState} from 'react';
import {View, Text, TextInput,Switch, TouchableOpacity} from 'react-native';
import Styles from '../Utils/Styles';
import color from '../Utils/Color';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Container from '../HOC/hocComponent';
import Btncomponent from '../Component/btncomponent'
import {ApiCall} from '../Api/Api';
import {Urls} from '../Api/ServerUrl';
import {useNavigation} from '@react-navigation/native';
import {useDispatch} from 'react-redux';
import TextInputComponent from '../Component/TextInputComponent'

const LoginComponent = (props) => {

      const Navigation = useNavigation()
    const [isEnabled, setIsEnabled] = useState(true);
    const [contrycode, setcontrycode] = useState('98');
    const [number, Setnumber] = useState('');
    const dispatch = useDispatch();



const sobmit = () =>{
  if(number.length > 0 == 0){
    alert('number 0 is wrong')
  }else{
    const body = {
      username:`${contrycode}${number}`
  } 
ApiCall(
'POSt',
Urls.login,
body,
'login',
null,
res => {
  if(res.statusCode == 200){
      dispatch({type:'LOGIN',token:res.data.userId})
       Navigation.navigate('Confirm',{item:`${contrycode}${number}`})
  }else{
      ApiCall(
          'POSt',
          Urls.users,
          body,
          'users',
          null,
          res => {
              res.statusCode == 201 ?alert('success') :null   

          },
          onError => {
              console.warn(onError.message)
          },
        );  
  }
},
onError => {
    console.warn(onError.message)
},
);
  }
      
}


  return (
    <View style={Styles.container}>
      <Text style={[Styles.margintop,Styles.alignsef,{color:color.gray}]}>
        {props.title}
      </Text>
      <Text style={[Styles.alignsef,{color:color.gray}]}>
        and enter your phone number
      </Text>
      <View
        style={[Styles.box,Styles.margintop]}>
         <Text style={{color:color.blue,paddingLeft:15}}>Iran</Text>
        <Icon
          name="keyboard-arrow-right"
          color={color.black}
          size={30}
        />
      </View>
      <View
        style={[Styles.box,Styles.margintop2,{backgroundColor:color.white}]}>
            <View style={Styles.box2}>
                <Text style={{fontSize:16,color:color.black}}>+98</Text>
            </View>
            <TextInputComponent changetextval={(val)=>Setnumber(val)}/>

    </View>

    <View
        style={[Styles.box,Styles.margintop,{backgroundColor:color.white}]}>
         <Text style={{color:color.gray,paddingLeft:15}}>sync contacts</Text>
         <Switch
        trackColor={{ false:'green', true: "green" }}
        thumbColor={isEnabled ? color.white : "#f4f3f4"}
        ios_backgroundColor="#3e3e3e"
        onValueChange={()=>setIsEnabled(!isEnabled)}
        value={isEnabled}
        
      />
      </View>
      <Btncomponent title='Sobmit' onclick={()=>sobmit()}/>
    </View>
  );
};

export default Container(LoginComponent);