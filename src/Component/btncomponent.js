import React,{useState} from 'react';
import {View, Text, TextInput,Switch, TouchableOpacity} from 'react-native';
import Styles from '../Utils/Styles';
import color from '../Utils/Color';
const btncomponent = (props) => {

  return (

      <TouchableOpacity style={[Styles.btn,Styles.margintop]} onPress={props.onclick}>
        <Text>{props.title}</Text>
      </TouchableOpacity>
  );
};

export default btncomponent