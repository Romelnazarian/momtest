import React from 'react';
import {View, Text, TextInput} from 'react-native';


const Container = (WrapComponent) => {
     const hocContainer = ({...props}) =>{
         return(
             <WrapComponent {...props} name='ali'/>
         )
     }
     return hocContainer
};

export default Container;
