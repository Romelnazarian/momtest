import React,{useState} from 'react';
import {View, Text, TextInput, TouchableOpacity} from 'react-native';
import Styles from '../Utils/Styles';
import color from '../Utils/Color';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {useSelector} from 'react-redux';
import Btncomponent from '../Component/btncomponent'
import TextInputComponent from '../Component/TextInputComponent'
import {ApiCall} from '../Api/Api';
import {Urls} from '../Api/ServerUrl';

const confirm = ({route}) => {
const apitoken = useSelector(state=>state.Auth.token)
const item = route.params.item
const [sendcodeval,Setsendcodeval] =useState()

const sobmit = () =>{
    const body = {
        username:'989392071214',
        password:sendcodeval
    } 
    // console.warn(body)
ApiCall(
  'POSt',
  Urls.sendcode,
  body,
  'sendcode',
  null,
  res => {
        console.log(res.user)
  },
  onError => {
      console.log('eror',onError?.message)
  },
);
}
 const repeadcode=()=>{
    const body = {
        username:'989392071214',
    } 
ApiCall(
  'POSt',
  Urls.login,
  body,
  'login',
  null,
  res => {
        // alert('success')
        console.warn(res)
  },
  onError => {
      console.warn(onError.message)
  },
);
 }


  return (

   <View style={Styles.container}>
       <Text style={[Styles.margintop2,Styles.alignsef,{color:color.black,fontSize:20}]}>
        + {item}
      </Text>
      <Text style={[Styles.margintop2,Styles.alignsef,{color:color.black,fontSize:16}]}>
        We have sent you an SMS with the code
      </Text>
      <View style={[Styles.box,Styles.margintop2,{backgroundColor:color.white,justifyContent:'center'}]}>
      <TextInputComponent inputstyle={{textAlign:'center',height:45,marginTop:20}} changetextval={(val)=>Setsendcodeval(val)} />
      </View>
      <TouchableOpacity onPress={()=>repeadcode()}>
      <Text style={[Styles.margintop,Styles.alignsef,{color:color.blue,fontSize:14}]}>
        Haven't received the code?
      </Text>
      </TouchableOpacity>

      <Btncomponent title='Sobmit' onclick={()=>sobmit()}/>

   </View>
    );
};

export default confirm;
