import {persistCombineReducers} from 'redux-persist';
import AsyncStorage from '@react-native-async-storage/async-storage';

const initialState = {
  islogin: false,
  token: '',
};

function Auth(state = initialState, action) {
  switch (action.type) {
    case 'LOGIN':
      return {
        ...state,
        token: action.token,
        islogin: action.islogin,
      };
    default:
      return state;
  }
}

const persistconfig = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: ['Auth'],
};

const AppReduser = persistCombineReducers(persistconfig, {
  Auth,
});

export default AppReduser;
