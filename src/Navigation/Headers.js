import React from 'react';

import {
  View,
  Text,
  SafeAreaView,
  StatusBar,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import color from '../Utils/Color';
import {useNavigation} from '@react-navigation/native';
import Styles from '../Utils/Styles';
const Headerscomponent = ({title, hide,discription,arrow,type}) => {
  const Navigation = useNavigation();
  
console.warn(type)

  return (

       <SafeAreaView>
                 <StatusBar backgroundColor={color.gray} />
         {hide === true && (
        <View style={[Styles.rowtotal,{backgroundColor:color.lightwhite,height:60,borderBottomColor:color.lightwhite}]}>
          {arrow == true ?
                    <TouchableOpacity onPress={() => Navigation.goBack()}>
                    <Icon
                      name="keyboard-arrow-left"
                      color={color.black}
                      style={{
                        marginHorizontal: 10,
                        alignSelf: 'flex-end',
                        marginTop: '2%',
                      }}
                      size={34}
                    />
                  </TouchableOpacity>
                  :
                  <Text></Text>
          }

         <Text style={{fontSize:16}}>{title}</Text>
         <TouchableOpacity>
         <Text style={[{paddingRight:20,color:type===true?color.blue:color.black}]}>{discription}</Text>
         </TouchableOpacity>

        </View>
      )}
       </SafeAreaView>
  );
};

export default Headerscomponent;
