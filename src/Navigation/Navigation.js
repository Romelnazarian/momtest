import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
const Stack = createStackNavigator();

//-----------------login-------------------//
import Login from '../Screen/Login';
import Confirm from '../Screen/confirm';


import Headerscomponent from './Headers';

const AppNavigator = () => {
  return (
    <Stack.Navigator>
  

            <Stack.Screen
        component={Login}
        name="Splash"
        options={{
          header: () => <Headerscomponent hide={true} arrow={true} title='Your phone' discription='Next' />,
        }}
      />
              <Stack.Screen
        component={Confirm}
        name="Confirm"
        options={{
          header: () => <Headerscomponent hide={true} arrow={true} title='Confirmation' discription='Next' type={true}/>,
        }}
      />

    </Stack.Navigator>
  );
};

export default AppNavigator;
